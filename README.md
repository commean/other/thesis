# Diploma Thesis

This repository contains the Diploma Thesis for Commean.

Also, this repository is forked from the [HTL Mössingerstraße Latex Diplomathesis template](https://git.htl-klu.at/htl/htlthesis).


## Commands

### One size fits all

To make the cross-references work multiple commands are needed. But it is also possible to use `latexmk` which should take care of it all.

```
latexmk -pdf -output-directory=build -aux-directory=build -shell-escape main
```

To generate the `.tex` files from `.ods` spreadsheets:

```
ssconvert --export-type=Gnumeric_html:latex $IN $OUT
```

### Texmaker

But if you want to do the steps manually, you can use the following commands:
To generate all files in the build folder instead of littering the same directory as the `.tex` these commands can be used:

LaTeX: `latex -interaction=nonstopmode -output-directory=build %.tex`

PdfLaTeX: `pdflatex -synctex=1 -interaction=nonstopmode --output-directory=build %.tex`

Or if using [Texmaker](https://www.xm1math.net/texmaker/) the checkbox (`Use a "build" subdirectory for output files`) can be checked.


#### In general

```
latex -interaction=nonstopmode -output-directory=build %.tex
makeglossaries -d build %
latex -interaction=nonstopmode -output-directory=build %.tex
```

#### For main.tex

```
pdflatex -synctex=1 -interaction=nonstopmode -output-directory=build main.tex
makeglossaries -d build main
pdflatex -synctex=1 -interaction=nonstopmode -output-directory=build main.tex
```

#### For Texmaker as a user command

```
pdflatex -synctex=1 -interaction=nonstopmode -output-directory=build %.tex | makeglossaries -d build % | pdflatex -synctex=1 -interaction=nonstopmode -output-directory=build %.tex
```

## GitLab Runner

Create a folder for the GitLab Runner and use docker to start it:

```
docker run -d --name gitlab-runner --restart always \
    -v /var/run/docker.sock:/var/run/docker.sock \
    -v gitlabRunnerConfig:/etc/gitlab-runner \
    gitlab/gitlab-runner:latest
```

And to register it, use this command:

```
docker run --rm -it -v gitlabRunnerConfig:/etc/gitlab-runner gitlab/gitlab-runner:latest register
```


The original README.md:

HTL Mössingerstraße Latex Diplomathesis template
================================================

Empfohlene Zusatzsoftware:
--------------------------
* bibliography file (.bib) editor Jabref: https://github.com/JabRef/


Wichtige Informationen:
-----------------------

Vergessen Sie nicht am Ende der Arbeit die Editor Informatioenen auszublenden. U.a. durch entfernen von ***\linenumbers*** aus der ***main.tex***.
