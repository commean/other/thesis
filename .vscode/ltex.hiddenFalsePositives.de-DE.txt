{"rule":"GERMAN_SPELLER_RULE","sentence":"^\\QAls Programmiersprache für die Fahrzeug-Erkennung- & Klassifizierung stehen im Prinzip viele Programmiersprachen offen.\\E$"}
{"rule":"DE_CASE","sentence":"^\\Q(Das passiert aber so nicht, mehr dazu später.) Da so ein künstliches Neutron nicht viel alleine ausrichten kann, wird es in ein Netzwerk mit vielen anderen Neutronen gepackt.\\E$"}
{"rule":"GERMAN_SPELLER_RULE","sentence":"^\\QHier ist noch anzumerken, dass hier, bei der Objekt-Erkennung meist nicht einfach ein sogenanntes \\E(?:Dummy|Ina|Jimmy-)[0-9]+\\Q neuronales Netzwerk, welches bei „par:what_are_neuronal_networks“ \\E(?:Dummy|Ina|Jimmy-)[0-9]+\\Q beschrieben wurde, ausreicht.\\E$"}
{"rule":"DE_SENTENCE_WHITESPACE","sentence":"^\\QLauncher“ über den Task-Manager gestartet werden:\\E$"}
{"rule":"GERMAN_SPELLER_RULE","sentence":"^\\QEs muss im Prinzip nur der Dienst „WinFsp.\\E$"}
{"rule":"GERMAN_SPELLER_RULE","sentence":"^\\QEinmal /mnt und /run/media (bzw. früher nur /media).\\E$"}
{"rule":"GERMAN_SPELLER_RULE","sentence":"^\\QDa thunar, ein File-Manager, Geräte, welche unter /run/media anzeigt und durch einen Knopf-Druck „ausgeworfen“, bzw. in diesem Fall einfach nur getrennt werden, wird hier dieser Ort bevorzugt, jedoch gilt das selbe Prinzip, falls die Verbindung in den /mnt Ordner erfolgen soll.\\E$"}
{"rule":"GERMAN_SPELLER_RULE","sentence":"^\\QDa thunar, ein File-Manager, Geräte, welche unter /run/media anzeigt und durch einen Knopf-Druck „ausgeworfen“, bzw. in diesem Fall einfach nur getrennt werden, wird hier dieser Ort bevorzugt, jedoch gilt dasselbe Prinzip, falls die Verbindung in den /mnt Ordner erfolgen soll.\\E$"}
{"rule":"GERMAN_SPELLER_RULE","sentence":"^\\QIn diesem Fall wird der Name raspi1 verwendet.\\E$"}
{"rule":"GERMAN_SPELLER_RULE","sentence":"^\\Q$USER und $name werden automatisch durch den Namen des aktuellen Benutzerkontos und durch den Namen, welcher zuvor definiert wurde, ersetzt.\\E$"}
{"rule":"LEERZEICHEN_VOR_AUSRUFEZEICHEN_ETC","sentence":"^\\QHierbei steht der Wert vor dem : für den/die Benutzer*in, welche* der Ordner gehört und der Wert nach diesem, für die Gruppe.\\E$"}
{"rule":"GERMAN_SPELLER_RULE","sentence":"^\\QNach dem Neustart sollten sich unter /dev/video* neue „Dateien“ befinden.\\E$"}
{"rule":"GERMAN_SPELLER_RULE","sentence":"^\\QAus der Ausgabe kann geschlossen werden, dass es einige Devices gibt, jedoch haben nur root und die Gruppe video darauf Lese- & Schreibrechte.\\E$"}
{"rule":"GERMAN_SPELLER_RULE","sentence":"^\\QUm dieses Problem zu lösen, können entweder die Berechtigungen der Datei so geändert werden, dass alle User*innen darauf Zugriff haben, oder dass alle User*innen, welche darauf Zugriff haben sollen, in die Gruppe video hinzugefügt werden.\\E$"}
{"rule":"GERMAN_SPELLER_RULE","sentence":"^\\QUm Ersteres zu erreichen kann mit chmod other, also allen anderen außer root & der Gruppe video, die Berechtigungen wie folgt geben:\\E$"}
{"rule":"GERMAN_SPELLER_RULE","sentence":"^\\QUm Ersteres zu erreichen, kann mit chmod other, also allen anderen außer root & der Gruppe video, die Berechtigungen wie folgt geben:\\E$"}
{"rule":"GERMAN_SPELLER_RULE","sentence":"^\\QDas Hinzufügen eines Accounts zu einer Gruppe kann mittels usermod erfolgen.\\E$"}
{"rule":"GERMAN_SPELLER_RULE","sentence":"^\\QSo wird durch folgenden Command dietpi zu der Gruppe video hinzugefügt:\\E$"}
{"rule":"DE_AGREEMENT","sentence":"^\\QDann kann cheese entweder über die CLI oder über das Startmenü des jeweiligen Desktop Environments gestartet werden.\\E$"}
{"rule":"DE_CASE","sentence":"^\\QFalsches Seitenverhältnis Zu hohe Auflösung (2560x1920) Falsche Einstellungen der Auflösung\\E$"}
{"rule":"DOPPELTE_SATZZEICHEN","sentence":"^\\QWas sind neuronale Netzwerke?.\\E$"}
{"rule":"DOPPELTE_SATZZEICHEN","sentence":"^\\QWie lernt das neuronale Netzwerk?.\\E$"}
{"rule":"DOPPELTE_SATZZEICHEN","sentence":"^\\QWas ist ein Datensatz?.\\E$"}
{"rule":"GERMAN_SPELLER_RULE","sentence":"^\\QHierbei wird die Root-Directory des Remote-Hosts unter /run/media/$USER/$name eingebaut.\\E$"}
{"rule":"GERMAN_SPELLER_RULE","sentence":"^\\QDas Ziel der Diplomarbeit ist es die, in der cha:introduction (\\E(?:Dummy|Ina|Jimmy-)[0-9]+\\Q) beschriebene, Ausgangslage zu verbessern.\\E$"}
{"rule":"PRAEP_DAT","sentence":"^\\QNun sollte die Webcam nicht nur von root verwendet werden können, sondern auch von eine* Benutzer*in ohne \\E(?:Dummy|Ina|Jimmy-)[0-9]+\\Q.\\E$"}
{"rule":"GERMAN_SPELLER_RULE","sentence":"^\\QEinerseits gibt es ein sogenanntes (AML-NPU-)SDK, wobei es nicht wirklich eines ist, sondern nur Programmcode, welcher abgeändert werden muss.\\E$"}
{"rule":"UPPERCASE_SENTENCE_START","sentence":"^\\Qpip name=pip, description=Pip ist ein/der \\E(?:Dummy|Ina|Jimmy-)[0-9]+\\Q für Python.\\E$"}
{"rule":"GERMAN_SPELLER_RULE","sentence":"^\\QUnter dem Ordner examples/darknet \\E(?:Dummy|Ina|Jimmy-)[0-9]+\\Q befinden sich dann auch schon zusätzlich ein YOLOv3 Netzwerk, welches auf das Dateiformat, welches KSNN benötigt, um konvertiert ist.\\E$"}
{"rule":"DIES_DIE","sentence":"^\\QNun ist es möglich die Pakete pro Benutzer*in zu installieren, jedoch hat auch dies Nachteile.\\E$"}
{"rule":"GERMAN_SPELLER_RULE","sentence":"^\\QNun wurde die virtuelle Umgebung „nur“ erstellt, um in diese jedoch zu aktivieren, muss vorher eine gewisse Datei in der aktuellen Shell mit source ausgeführt werden.\\E$"}
{"rule":"LEERZEICHEN_NACH_VOR_ANFUEHRUNGSZEICHEN","sentence":"^\\QBeim Verbinden mit dem VNC-Server wird automatisch eine bestimmte Datei, welche die grafische Umgebung initialisieren soll, ausgeführt ( /.vnc/xstartup).\\E$"}
{"rule":"GERMAN_SPELLER_RULE","sentence":"^\\QBeim Verbinden mit dem VNC-Server wird automatisch eine bestimmte Datei, welche die grafische Umgebung initialisieren soll, ausgeführt ( /.vnc/xstartup).\\E$"}
{"rule":"GERMAN_SPELLER_RULE","sentence":"^\\Q[language=bash, caption=xstartup-Datei zum Initialisieren der Desktop-Umgebung]source/vnc/xstartup\\E$"}
{"rule":"GERMAN_SPELLER_RULE","sentence":"^\\QHier werden zuerst die zwei Umgebungsvariablen (SESSION_MANAGER und DBUS_SESSION_BUS_ADDRESS) gelöscht und dann die Xresources, falls vorhanden, ausgeführt.\\E$"}
{"rule":"GERMAN_SPELLER_RULE","sentence":"^\\QZu guter Letzt wird dann die Desktop-Umgebung mit exec startlxde gestartet.\\E$"}
{"rule":"GERMAN_SPELLER_RULE","sentence":"^\\QHier werden zuerst die zwei Umgebungsvariablen gelöscht und dann die Xresources, falls vorhanden, geladen.\\E$"}
{"rule":"GERMAN_SPELLER_RULE","sentence":"^\\Q[language=bash, caption=Skript zum Starten des VNC-Servers]source/vnc/vncAtHome.sh\\E$"}
{"rule":"GERMAN_SPELLER_RULE","sentence":"^\\Q[language=bash, caption=Skript zum Starten des VNC-Servers, label=code:start_vnc_server]source/vnc/vncAtHome.sh\\E$"}
{"rule":"GERMAN_SPELLER_RULE","sentence":"^\\Q[language=bash, caption=Skript zum Starten des VNC-Servers, label=code:start_vnc_server]source/vnc/vncAtHome.sh [language=bash, caption=Ausgabe des Skripts]source/vnc/vncAtHomeOut\\E$"}
{"rule":"COMMA_PARENTHESIS_WHITESPACE","sentence":"^\\QTheoretisch ist es mit Python möglich einfach alles eine .py-Datei zu packen.\\E$"}
{"rule":"DE_AGREEMENT","sentence":"^\\QDa der \\E(?:Dummy|Ina|Jimmy-)[0-9]+\\Q jeden Pixel verfolgt, benötigt dieser auch einiges an Leistung.\\E$"}
{"rule":"UPPERCASE_SENTENCE_START","sentence":"^\\Qxstartup-Datei zum Initialisieren der Desktop-Umgebung\\E$"}
{"rule":"DE_AGREEMENT","sentence":"^\\QAufgrund der freien Frequenzenbereichen der beliebigen Staaten darf man sein eigenes Gateway aktivieren.\\E$"}
{"rule":"DE_CASE","sentence":"^\\QZum Vergleich: Frequenzbänder von Österreich \\E(?:Dummy|Ina|Jimmy-)[0-9]+\\Q Wieso ist LTE für 5G wichtig?.\\E$"}
{"rule":"DE_COMPOUND_COHERENCY","sentence":"^\\QAls Wi-Fi bezeichnet man die drahtlose Technologie, mit der Endgeräte miteinander über das Internet verbunden werden.\\E$"}
{"rule":"GERMAN_SPELLER_RULE","sentence":"^\\Q\\E(?:Dummy|Ina|Jimmy-)[0-9]+\\Q JSON name=JSON, description=„JSON ist ein allgemeines Datenformat mit einer minimalen Anzahl von Werttypen: Strings, numbers, booleans, lists, objects und null.\\E$"}
{"rule":"UPPERCASE_SENTENCE_START","sentence":"^\\Qinnovation@school.\\E$"}
{"rule":"DE_AGREEMENT","sentence":"^\\QTechnik fürs Leben-Preis 2022.\\E$"}
{"rule":"DE_CASE","sentence":"^\\QJugend Innovativ.\\E$"}
{"rule":"DE_AGREEMENT","sentence":"^\\QSind dann bei einer User Story alle Tasks abgeschlossen, kann auch die User Story als abgeschlossen betrachtet werden.\\E$"}
