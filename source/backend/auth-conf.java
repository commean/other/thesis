
public class AuthConfig extends WebSecurityConfigurerAdapter {

	@Override
	protected void configure(HttpSecurity http) throws Exception {

		http.headers()
			.frameOptions().sameOrigin()
			.and()
			.csrf().disable()
			.authorizeRequests()
			.antMatchers("/api/v1/auth/**").permitAll()
			.antMatchers(HttpMethod.POST, "/api/v1/measurements/**").authenticated()
			.antMatchers(HttpMethod.PUT, "/api/v1/nodes").authenticated()
			.and()
			.sessionManagement().sessionCreationPolicy(SessionCreationPolicy.STATELESS)
			.and()
			.addFilterBefore(jwtAuthenticationFilter(), UsernamePasswordAuthenticationFilter.class);

	}
}
