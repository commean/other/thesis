@Repository
public interface NodeRepository extends CrudRepository<Node, UUID> {
	Iterable<Node> findAllByCrossroad(Crossroad crossroad);

	Iterable<Node> findAllWhereLocationNotNull();
}
