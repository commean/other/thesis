public interface NodeService {

	Node addNode(Node tcn);
	List<Node> getAllNodes();
	Node getNodeById(UUID id);
	List<Node> getNodesByCrossroad(Crossroad c);
	List<Node> getAllNodesWhereLocatioNotNull();
	void deleteNodeById(UUID id);
}
