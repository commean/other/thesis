
@Component
@Log4j2
public class JwtProvider {

	public String generateToken(Authentication authentication) {
		UserPrincipal userPrincipal = (UserPrincipal) authentication.getPrincipal();
		Timestamp expDate = Timestamp.from(Instant.now().plus(jwtExpirationInDays, ChronoUnit.DAYS));

		return Jwts.builder()
				.setSubject(userPrincipal.getUsername())
				.setIssuedAt(Date.from(Instant.now()))
				.setExpiration(expDate)
				.signWith(getSecretKey())
				.compact();
	}
}
