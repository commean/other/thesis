@Service
public class NodeServiceImpl implements NodeService {

	NodeRepository cameraNodeRepository;

	@Autowired
	public NodeServiceImpl(NodeRepository cameraNodeRepository) {
		this.cameraNodeRepository = cameraNodeRepository;
	}
	@Override
	public Node addNode(Node tcn) {
		return cameraNodeRepository.save(tcn);
	}
    //...
	@Override
	public void deleteNodeById(UUID id) {
		cameraNodeRepository.deleteById(id);

	}

}
