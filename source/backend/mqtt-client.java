public static void create() {
		if (Client.asyncClient == null) {
			try {
				asyncClient = new MqttAsyncClient(props.getTtnUrl(), props.getClientId());
				MqttConnectOptions connOpts = new MqttConnectOptions();
				connOpts.setCleanSession(true);
				connOpts.setAutomaticReconnect(true);
				connOpts.setUserName("%s@ttn".formatted(props.getTtnAppId()));
				connOpts.setPassword(props.getTtnKey());
				MqttToken token = (MqttToken) asyncClient.connect(connOpts);
				token.waitForCompletion(5000);
				log.info("MQTT connected");
			} catch (MqttException e) {
				log.error("MQTT connection failed \n{}", e.toString());
			}}
		