@RestController
@RequestMapping("/api/v1/nodes")
public class NodeController {

	@GetMapping(value = "", produces = "application/json")
	@ResponseStatus(code = HttpStatus.OK)
	public Object getNode(@RequestParam("id") UUID uuid) {
		return NodeDto.convertToDto(tcns.getNodeById(uuid));
	}
    //...
}
