public class UserPrincipal implements UserDetails {

	private String username;
	private String password;
	private boolean enabled;
	private Collection<? extends GrantedAuthority> authorities;

	public static UserPrincipal userToPrincipal(User user) {
		UserPrincipal userPrincipal = new UserPrincipal();
		List<SimpleGrantedAuthority> authorities = user.getRoles().stream()
				.map(role -> new SimpleGrantedAuthority("ROLE_" + role.getName())).collect(Collectors.toList());

		userPrincipal.setUsername(user.getUsername());
		userPrincipal.setPassword(user.getPassword());
		userPrincipal.setEnabled(user.getActive());
		userPrincipal.setAuthorities(authorities);
		return userPrincipal;
	}
    //Getters and Setters
}
