public Authentication authenticate(Authentication authentication) throws AuthenticationException {

		String principal = (String) authentication.getPrincipal();

		if (apiKeyService.checkForApiKey(principal) != 1) {
			throw new BadCredentialsException("The API key was not found or not the expected value.");
		} else {
			authentication.setAuthenticated(true);
			log.debug("Auth Successful");
			return authentication;
		}


	}
