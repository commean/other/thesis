@Entity
@Table(name = "nodes")

@EnableAutoConfiguration

public class Node {
	@Id
	@GenericGenerator(name = "uuid2", strategy = "uuid2")
	private UUID id;

	private String location;

	@ManyToOne

	@JoinColumn(name = "crossroad_id", referencedColumnName = "id")
	private Crossroad crossroad;

    //Getter and Setters