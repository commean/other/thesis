
	@Secured("admin")
	@PostMapping(value = "/signup", consumes = "application/json", produces = "application/text")
	public ResponseEntity<?> registerUser(@RequestBody SignUpDto signUpDto) {

		if (userRepository.existsByUsername(signUpDto.getUsername())) {
			return new ResponseEntity<>("Username already taken!", HttpStatus.BAD_REQUEST);
		}
		User user = new User();
		user.setUsername(signUpDto.getUsername());
		user.setPassword(passwordEncoder.encode(signUpDto.getPassword()));
		user.setActive(true);

        if (signUpDto.getRole()==null)
		Role roles = roleRepository.findByName(RoleName.NODE);
        else
        Role roles = roleRepository.findByName(signUpDto.getRole());
        if (roles != null)
		user.setRoles(Arrays.asList(roles));

		userRepository.save(user);
		return new ResponseEntity<>("User registered successfully", HttpStatus.CREATED);
	}
}
