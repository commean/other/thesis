    def generate_img(self) -> cv.cvtColor:
        mag, ang = cv.cartToPolar(self.flow[..., 0], self.flow[..., 1])
        self.hsv[..., 0] = ang * 180 / np.pi / 2
        self.hsv[..., 2] = cv.normalize(mag, None, 0, 255, cv.NORM_MINMAX)

        return cv.cvtColor(self.hsv, cv.COLOR_HSV2BGR)