    def get_direction_of_rect(self, box: Box, fps: int) -> OpticalFlowValue:
        """Given a specific portion of the frame, this function
           calculates the optical flow of this area.

        :param box: Area on which the optical flow should be
                    calculated
        :type box: Box
        :param fps: Frames per Second
        :type fps: int
        :return: An object which encapsulates the optical flow
        :rtype: OpticalFlowValue
        """
        left, top, right, bottom = box.get_values()
        cropped_flow = self.flow[top:bottom, left:right, :] / fps

        flow_sum = np.sum(np.sum(cropped_flow, axis=0), axis=0)
        std_x = np.std(cropped_flow[:, :, 0])
        std_y = np.std(cropped_flow[:, :, 1])

        return OpticalFlowValue(flow_sum[0], flow_sum[1]), (std_x, std_y)