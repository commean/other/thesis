calcOpticalFlowFarneback (  InputArray          prev,
                            InputArray          next,
                            InputOutputArray    flow,
                            double              pyr_scale,
                            int                 levels,
                            int                 winsize,
                            int                 iterations,
                            int                 poly_n,
                            double              poly_sigma,
                            int                 flags 
                         )         