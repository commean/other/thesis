    def add_vectors_to_img(self, img: np.ndarray, spacing=8) -> None:
        img_height = img.shape[0]
        img_width = img.shape[1]

        y = 0
        for flow_x in self.flow:
            x = 0
            if y % spacing == 0:
                for flow_y in flow_x:
                    if x % spacing == 0:
                        mag_x = flow_y[0]
                        mag_y = flow_y[1]

                        end_x = int(x + mag_x)
                        end_y = int(y + mag_y)
                        end_x = end_x if 0 <= end_x and end_x <= img_width else x
                        end_y = end_y if 0 <= end_y and end_y <= img_height else y
                        cv.line(img, (x, y), (end_x, end_y), (0, 255, 0), 1)
                    x += 1
            y += 1