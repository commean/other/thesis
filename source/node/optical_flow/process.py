class OpticalFlow:
    def init(self, frame: np.ndarray) -> None:
        """Initializes Optical Flow"""
        self.previous = cv.cvtColor(frame, cv.COLOR_BGR2GRAY)
        self.hsv = np.zeros_like(frame)
        self.hsv[..., 1] = 255

    def generate_flow(self, img: np.ndarray) -> None:
        self.next = cv.cvtColor(img, cv.COLOR_BGR2GRAY)
        self.flow = cv.calcOpticalFlowFarneback(
            self.previous, self.next, None, 1 / 2, 4, 30, 3, 5, 1.1, 0
        )
        self.previous = self.next