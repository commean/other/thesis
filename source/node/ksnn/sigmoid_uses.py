    @staticmethod
    def _process(input, mask, anchors):
        anchors = [anchors[i] for i in mask]
        grid_h, grid_w = map(int, input.shape[0:2])

        box_confidence = YoloPostProcessing.faster_sigmoid(input[..., 4])
        box_confidence = np.expand_dims(box_confidence, axis=-1)

        box_class_probs = YoloPostProcessing.faster_sigmoid(input[..., 5:])

        box_xy = YoloPostProcessing.sigmoid(input[..., :2])
        box_wh = np.exp(input[..., 2:4])
        box_wh = box_wh * anchors
        ...