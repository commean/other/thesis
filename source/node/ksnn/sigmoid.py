    @staticmethod
    def sigmoid(x):
        return 1 / (1 + np.exp(-x))

    @staticmethod
    def faster_sigmoid(x):
        return np.where(x > 1, 1 / (1 + np.exp(-x)), 0)