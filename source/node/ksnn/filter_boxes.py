    @staticmethod
    def _filter_boxes(boxes, box_confidences, box_class_probs):
        box_scores = box_confidences * box_class_probs
        box_classes = np.argmax(box_scores, axis=-1)
        box_class_scores = np.max(box_scores, axis=-1)
        pos = np.where(
            np.logical_and(
                box_class_scores >= YoloPostProcessing.OBJ_THRESH,
                (box_classes == 2)  # car
                | (box_classes == 3)  # motorbike
                | (box_classes == 5)  # bus
                | (box_classes == 7),  # truck
            )
        )

        boxes = boxes[pos]
        classes = box_classes[pos]
        scores = box_class_scores[pos]

        return boxes, classes, scores