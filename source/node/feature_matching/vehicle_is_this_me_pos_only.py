    def is_this_me(self, vehicle_detected: Vehicle, matches: tuple):
        if type(vehicle_detected) != type(self):
            self.logger.info("Invalid type!")
            return sys.maxsize

        wrong_class_penalty = 1
        if vehicle_detected.get_object_class() != self.get_object_class():
            self.logger.debug(
                "Invalid object class! %s - %s",
                vehicle_detected.get_object_class(),
                self.get_object_class(),
            )
            wrong_class_penalty = 1.5

        my_center = self.get_center()
        their_center = vehicle_detected.get_center()

        diff = tuple(map(lambda i, j: i - j, my_center, their_center))
        length = np.linalg.norm(diff)

        distance_sum = 0
        for match in matches:
            distance_sum += match.distance

        return (
            distance_sum * wrong_class_penalty * length * 7 / (len(matches) * 10000)
        ) ** 2
