    def is_this_me(self, vehicle_detected: Vehicle, matches: tuple):
        ...
        predicted_next_time = TimePassed.get_next_time()
        their_center = vehicle_detected.get_center()
        my_predicted_center = (
            self.multiple_regression.get_x(predicted_next_time),
            self.multiple_regression.get_y(predicted_next_time),
        )

        diff = tuple(map(lambda i, j: i - j, my_predicted_center, their_center))
        length = np.linalg.norm(diff)
        ...
        return (
            distance_sum * wrong_class_penalty * length / (len(matches) * 1000)
        ) ** 2