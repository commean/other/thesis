import numpy as np
from numpy.polynomial import Polynomial

class MultipleRegression:
    def __init__(self, x: np.array, y: np.array, time_now: np.array):
        self.model_x = Polynomial.fit(time_now, x, 3, domain=[0, 3000])
        self.model_y = Polynomial.fit(time_now, y, 3, domain=[0, 3000])

    def get_x(self, time_now: np.array):
        return self.model_x(time_now)

    def get_y(self, time_now: np.array):
        return self.model_y(time_now)